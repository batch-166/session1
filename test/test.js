const { assert } = require('chai');
const { newUser } = require('../index.js');


describe('Test newUser object', () => {
    //specifies certain test case
    it('Assert newUser type is an object', () => {
        assert.equal(typeof(newUser), 'object')
    });
    it('Assert newUser email is type string', () => {
        assert.equal(typeof(newUser.email), 'string')
    });
    it('Assert newUser email is not undefined', () => {
        assert.notEqual(typeof(newUser.email), 'undefined')
    });
    it('Assert newUser password is type string', () => {
        assert.equal(typeof(newUser.password), 'string')
    });
    it('Assert newUser password has atleast 16 characters', () => {
        assert.isAtLeast(newUser.password.length, 16)
    });
    it('Assert newUser first name type is a string', () => {
        assert.equal(typeof(newUser.firstName), 'string')
    })
    it('Assert newUser last name type is a string', () => {
        assert.equal(typeof(newUser.lastName), 'string')
    })
    it('Assert newUser first name is not undefined', () => {
        assert.notEqual(typeof(newUser.firstName), 'undefined')
    })
    it('Assert newUser last name type is a string', () => {
        assert.notEqual(typeof(newUser.lastName), 'undefined')
    })
    it('Assert newUser age type is a number', () => {
        assert.equal(typeof(newUser.age), 'number')
    })
    it('Assert newUser age is at least 18', () => {
        assert.isAtLeast(newUser.age, 18)
    })
    it('Assert newUser contact number type is a string', () => {
        assert.equal(typeof(newUser.contactNo), 'string')
    })
    it('Assert newUser batch number type is a number', () => {
        assert.equal(typeof(newUser.batchNo), 'number')
    })
    it('Assert newUser batch number is not undefined', () => {
        assert.notEqual(typeof(newUser.batchNo), 'undefined')
    });
});